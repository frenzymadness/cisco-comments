#!/usr/bin/env python3

import sys
import json
import glob
import hashlib
import socket
import re
import os

# Settings
database = os.path.dirname(__file__) + '/comments.db' # File to store database
config_dir = os.path.dirname(__file__) + '/configs/'  # Folder with Cisco config files
config_suffix = '*.config'                            # Cisco config files suffix
comment_splitter = '#'                                # Comments splitter character
dns_splitter = "| "                                   # DNS splitter character


def save(dnslookup=False):
    '''Save comment from config files to database'''
    comments = {}
    # For every line in every config file
    for configfile in glob.glob(config_dir + config_suffix):
        with open(configfile, 'r') as fh:
            for line in fh:
                # Get data and comments part separated and calculate md5 hash
                data = line.split(comment_splitter)
                md5 = hashlib.md5(data[0].strip().encode('utf-8')).hexdigest()

                # If line contains comment, save it to dict
                if len(data) > 1:
                    comments[md5] = data[1].strip()

                # If enabled - make dnslookup and add information to comment
                if dnslookup:
                    # If line already contains dns separator, skip them
                    if line.find(dns_splitter) != -1:
                        continue
                    # Finding ip in line and get hostname for all ip found
                    ip = re.compile(r"\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b")
                    for address in ip.findall(line):
                        # Exclude mask IP address
                        if address.startswith('0.') or address.startswith('255.'):
                            continue
                        # Try to get hostname from IP
                        try:
                            if md5 in comments:
                                comments[md5] += dns_splitter + socket.gethostbyaddr(address)[0]
                            else:
                                comments[md5] = socket.gethostbyaddr(address)[0]
                        except:
                            pass

    # If we have empty dict, rather do nothing instead truncate database.
    if len(comments):
        with open(database, 'w') as fh:
            fh.write(json.dumps(comments, sort_keys=True, indent=2))
    else:
        print('Config files not contains comments. Nothing saved instead truncate database. Try write comments from database to config files first.')


def load(filename=None):
    '''Load comments database and show configs with comments on stdout.'''
    with open(database, 'r') as db:
        comments = json.loads(db.read())

    for configfile in glob.glob(config_dir + config_suffix):
        # if we have filename specified skip all others files
        if filename is not None and not configfile.endswith(filename):
            continue

        with open(configfile, 'r') as fh:
            for line in fh:
                data = line.split(comment_splitter)
                md5 = hashlib.md5(data[0].strip().encode('utf-8')).hexdigest()
                if md5 in comments:
                    print('{:<80s} {}'.format(data[0].rstrip(), comment_splitter + ' ' + comments[md5]))
                else:
                    print(data[0].rstrip())


def write():
    '''Load comments database and replace configs with commented configs.'''
    with open(database, 'r') as db:
        comments = json.loads(db.read())

    for configfile in glob.glob(config_dir + config_suffix):
        with open(configfile, 'r') as fh:
            output = ""
            for line in fh:
                data = line.split(comment_splitter)
                md5 = hashlib.md5(data[0].strip().encode('utf-8')).hexdigest()
                if md5 in comments:
                    output += '{:<80s} {}'.format(data[0].rstrip(), comment_splitter + ' ' + comments[md5] + '\n')
                else:
                    output += data[0]

        with open(configfile, 'w') as fh:
            fh.write(output)


def clear():
    '''Remove comments from config files.'''
    for configfile in glob.glob(config_dir + config_suffix):
        with open(configfile, 'r') as fh:
            output = ""
            for line in fh:
                data = line.split(comment_splitter)
                output += data[0].rstrip() + '\n'

        with open(configfile, 'w') as fh:
            fh.write(output)


def printHelp():
    '''Only print help'''
    print('''
Usage: {}/comments.py save|load [filename]|write|clear|dnslookup

    save                    Save comments from config files to database.
    load [filename]         Load comments from database and print to stdout.
    write                   Load comments from database and write it to config files.
    clear                   Clear config files.
    dnslookup               Same as save but search for IPs and make dnslookup to store DNS names in comments.

Author: Lumír Balhar, frenzy.madness@gmail.com http://www.frenzy.cz/ @lumirbalhar
License: MIT
'''.format(os.path.dirname(__file__)))


if __name__ == '__main__':

    try:
        action = sys.argv[1]
    except:
        printHelp()
        sys.exit(2)

    # Save comments from file to database
    if action == 'save':
        save()
    # Load comments from database and print to stdout
    elif action == 'load':
        if len(sys.argv) > 2:
            load(filename=sys.argv[2])
        else:
            load()
    # Load comments from database and write it to config files
    elif action == 'write':
        write()
    # Clear config file from comments
    elif action == 'clear':
        clear()
    # Create comments with dnslookup
    elif action == 'dnslookup':
        save(dnslookup=True)
    else:
        printHelp()
