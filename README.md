# Comment Cisco configuration files
## Handling with comments in your Cisco configuration files
### Why?

Because Cisco doesn't support comments when you are configuring your device.

### When?

When you download your configuration file from Cisco device, it will be without comments. If you have a lot of config files, devices and lines of config your configuration maybe doesn't make sense.

### [My] workflow
#### First time

1. Download configuration files without comments from devices or from some backup storage.
2. Write some comments to config files to make it better readable.
3. Edit comments.py file and make changes in **Setting part - config file path, config file suffix etc.**
4. Run **./comments.py save** to save your comments to database.

Instead save, you can use:
> ./comments.py dnslookup

This also save your comments to database but try to resolve IPs founded in line to hostname and add this hostname to comment.

#### Next time when you download config files without comments you can
Load all stored comments and print config files with comments to stdout
> ./comments.py load

or write comments back to config files
> ./comments,py write

or remove comments from config files
> ./comments.py clear

### Important note
Script uses configuration line content as key to store comments - if you have two lines with same content theese lines will have same comments.

I think this is right behavior, because two lines with exactly same content mean same thing.

### Author
* Lumír Balhar
* frenzy.madness@gmail.com
* http://www.frenzy.cz/
* @lumirbalhar on Twitter

### If you find bug or have feature request, send it to me please.